const Geolocation = require("nativescript-geolocation");
const Accuracy = require("tns-core-modules/ui/enums");

let location = null

const fetchLocation = () => {
  Geolocation.getCurrentLocation({desiredAccuracy: Accuracy.high, updateDistance: 0.1, timeout: 20000}).then(loc => {
    if (loc) {
      location = location || {}
      location = {
        latitude: loc.latitude,
        longitude: loc.longitude
      }
    }
  })
}

Geolocation.enableLocationRequest()
  .then(() => {
    fetchLocation()
    setInterval(fetchLocation, 5000)
  });

export default {
  getLocation: () => location
}
