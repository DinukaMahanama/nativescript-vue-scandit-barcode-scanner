import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    message: { unread: false, text: 'asdf' }
  },
  mutations: {
    setUnread(state) {
      state.message.unread = false
      console.log(state.message.unread)
    }
  },
  actions: {

  }
})
