const firebase = require('nativescript-plugin-firebase')
const fbApp = require('nativescript-plugin-firebase/app')
let localStorage = require('nativescript-localstorage')
const plugin = require("nativescript-uuid");
import locationStuff from './locationStuff'

let fbToken
let fbInitialized = false
let deviceId

import store from './store'

let drivers
let name = localStorage.getItem('name')

const init = () => {
  console.log("fbStuff called");
  const uuid = plugin.getUUID();
  deviceId = localStorage.getItem('deviceId') || uuid
  localStorage.setItem('deviceId', deviceId)
  firebase
    .init(
      {
        persist: true,
        onMessageReceivedCallback: function (message) {
          console.log("Received firebase message: ", message.title, message.body)
          console.log('Data', message.data)
          console.log('Store: ' + store.state.message.text)
          store.state.message = {unread: true, text: message.data.body}
        },
        showNotificationsWhenInForeground: false,
        onPushTokenReceivedCallback: function (token) {
          console.log('Firebase push token: ', token)
        }
      }
    )
    .then(() => {
            drivers = fbApp.firestore().collection('drivers')
            fbInitialized = true
          },
          err => console.error('==============> Firebase Init failed: ', err)
    )
}

firebase.getCurrentPushToken().then(token => {
  fbToken = token
})

setInterval(() => {
  let loc = locationStuff.getLocation()
  if (loc && fbInitialized) {
    drivers.doc(deviceId).set({
                                name,
                                location: loc,
                                lastSeen: Date.now(),
                                fbToken
                              }, {merge: true})
  }

}, 5000)

export default {
  register (_name) {
    name = _name
    localStorage.setItem('name', name)
  },
  init,
  scanItem (time, code, value) {
    console.log("fbstuff scan Item");
    //let driver = drivers.doc(deviceId)
    let scan = {time, code, value}
    console.log("fbstuff scan Item 2");
    console.log('Updating scans with', scan)

   /* driver.update(
      {
        scans: fbApp.firestore().FieldValue().arrayUnion(JSON.stringify(scan))
      }
    ).then(() => {
      console.log('Drivers updated!')
    },
           (err) => console.error('SCAN UPDATE ERR: ', err)
           ).catch(err => console.error('SCANS UPDATE ERROR: ', err))
  */ }
}
