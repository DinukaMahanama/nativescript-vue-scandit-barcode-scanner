
export default {
  name: "ErrorMessages",
  data() {
    return {
      TimeFinal: 0,
      elapsed: 0,
      timer: false,
      running: false,
      start: Date.now(),
      timer: ""
    };
  },
  tick() {
    this.elapsed = new Date() - this.start;
  },
  startTimer() {
    let vm = this;
    clearInterval(this.timer);
    this.start = Date.now();
    this.timer = setInterval(vm.tick, 50);
    this.running = true;
  },
  resetTimer() {
    this.TimeFinal = (this.elapsed / 1000).toFixed(1);
    this.elapsed = 0;
    clearInterval(this.timer);
    this.running = false;
  },
  computed: {
    seconds() {
      return (this.elapsed / 1000).toFixed(1);
    }
  }
};
