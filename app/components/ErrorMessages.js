
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";

export default {

  showError1() {
    TNSFancyAlert.showError(
      "Scan Starting Barcode First",
      "You need to scan the barcode at the entrance to start the game",
      "Okay Got it!"
    )
      .then(() => {
        console.log("shown");
      })
      .catch(error => {
        console.error(error);
      });
  },
  showHelp() {
    TNSFancyAlert.showInfo(
      "How To Play",
      'Press "Start Game" To begin playing the game.\nScan The Barcode at the enternce to begin your hunt then find the barcode which will display the hidden message and the proceed to scan the barcode at the exit to quit the game',
      "Okay Got it!"
    )
      .then(() => {
        console.log("shown");
      })
      .catch(error => {
        console.error(error);
      });
  },
  FoundGreen() {
    TNSFancyAlert.showSuccess(
      "GREEN",
      "Du fant en grønn boks, fort deg til mål!",
      "Continue!"
    )
      .then(() => {
        console.log("shown");
      })
      .catch(error => {
        console.error(error);
      });
  },
  FoundRed() {
    TNSFancyAlert.customViewColor = "RED";
    TNSFancyAlert.showSuccess("RED", "RED Barcode Keep Playing On", "Continue!")
      .then(() => {
        console.log("shown");
      })
      .catch(error => {
        console.error(error);
      });
  },
  Finished(GCounted, RCounted, Time) {
    TNSFancyAlert.showSuccess(
      "You Finished",
      "Your Time is : " +
        Time +
        "\nGreen Barcodes Found : " +
        GCounted +
        "\nRed Barcodes Found : " +
        RCounted,
      "Okay"
    )
      .then(() => {
        console.log("shown");
      })
      .catch(error => {
        console.error(error);
      });
  }
};

