let permissions = require('nativescript-permissions')
const license = require('!raw-loader!!./license.txt')
const application = require("tns-core-modules/application")
import fbstuff from '../fbstuff'
import sc from "nativescript-barcodescanner"

function scanItem(time, code, value) {
  console.log("fbstuff scan Item");
  let scan = { time, code, value }
  console.log("fbstuff scan Item 2");
  console.log('Updating scans with', scan.value)
}

export default {
  init() {
    if (application.android) {
      let context = application.android.context

      com.scandit.barcodepicker.ScanditLicense.setAppKey(license)
      let settings = com.scandit.barcodepicker.ScanSettings.create()
      //Set Barcode Type here
      settings.setSymbologyEnabled(com.scandit.recognition.Barcode.SYMBOLOGY_CODE93, true)
      //settings.ScanSettings.setMatrixScanEnabled(true)
      let picker = new com.scandit.barcodepicker.BarcodePicker(context, settings)
      picker.getOverlayView().setViewfinderDecodedColor(234, 128, 252)

      picker.getOverlayView().setViewfinderColor(0, 0.8, 1)
      picker.getOverlayView().drawViewfinder(true)
      picker.getOverlayView().setViewfinderLandscapeDimension(25,30) 
      picker.setOnScanListener(new com.scandit.barcodepicker.OnScanListener(
        {
          didScan: function (scanSession) {
            let codes = scanSession.getNewlyRecognizedCodes().toArray()
            for (let i = 0; i < codes.length; i++) {
              let code = codes[i]
              let type = code.getSymbologyName()
              let data = code.getData()
              scanItem(Date.now(), type, data)
            }
          }
           	
        }))

      permissions.requestPermissions(
        [
          android.Manifest.permission.CAMERA,
          android.Manifest.permission.INTERNET,
          android.Manifest.permission.ACCESS_NETWORK_STATE
        ], "Permissions needed for barcode scanning and license verification")
        .then(() => {
          application.android.foregroundActivity.setContentView(picker)
          console.log("index line 46");
          picker.startScanning()
        }).catch((err) => {
          console.error("Some permission error", err)
        })
    } else if (application.ios) {
      //Magnus do your shit!
    }
  },
  methods:{

  }
}
